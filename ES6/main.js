/**
 * Aula/atividade do dia 09/09/2021 - PARTE 1
 */
const arr = [1,2,3,4,5,8,9]

const newArr = arr.map(function(item){
    return item * 2;
})

console.log("newArr: \n" + newArr + "\n")

const soma = arr.reduce(function(total, next){
    return total + next;
})

console.log("soma: \n" + soma + "\n")

const filter = arr.filter(function(item){
    return item % 2 == 0;
})

console.log("filter: \n" + filter + "\n")

const find = arr.find(function(item){
    return item == 4;
})

console.log("find: \n" + find + "\n")

const newArrComArrowFunction = arr.map(item => item*2)

const newArrSemArrowFunction = arr.map(function(item){
    return item * 2;
})

console.log("newArrComrrowFunction: \n" + newArrComArrowFunction + "\n")
console.log("newArrSemArrowFunction: \n" + newArrSemArrowFunction + "\n")

const teste = () => {return {nome: 'Wesley'}}

console.log("teste: ", teste(), "\n")

function soma1(a,b){
    return a+b;
}

function soma2(a=1, b=2){
    return a+b;
}

console.log("soma1: " + soma1(1,2))
console.log("soma2: " + soma2(1,5))

/**
 * Aula/atividade do dia 09/09/2021 - PARTE 2
 */

 const usuario = {
    nome: "Wesley",
    idade: 21,
    endereco: {
        rua: "Rua e",
        cidade: "Lafaiete Coutinho"
    }
}

// const nome = usuario.nome
// const idade = usuario.idade

// const {nome, idade, endereco:{cidade}} = usuario;
// console.log("idade: ", idade)

// console.log("idade: ", idade, "\nnome: ", nome, "\ncidade: ", cidade, "\n")

function mostraNome(usuario){
    console.log("mostraNome: ", usuario.nome, "\n")
}

function mostraNome2({nome}){
    console.log("mostraNome2: ", nome, "\n")
}

mostraNome(usuario)
mostraNome2(usuario)

const {nome, ...resto} = usuario
console.log("nome: ", nome, "\n")
console.log("resto: ", resto, "\n")

const arr1 = [1,2,3,4]
const [...c] = arr1;

console.log("c: ", c, "\n")

function soma3(a,b,c,d){
    return a+b+c+d
}

console.log("soma3: ", soma3(1,2,3,4), "\n")

function soma4(...params){
    console.log("params: ", params, "\n")
    return params.reduce((total,next) => total+next);
}

console.log("soma4: ", soma4(1,2,3,4), "\n")

const usuario1 = {
    nome: "Wesley",
    idade: 38,
    empresa: "UESB"
}

const usuario2 = {...usuario1, nome: "Marcos"}
console.log("Usuario2: ", usuario2, "\n")

const idade = 21
const name = "Wesley"

console.log('Meu nome é ' + name + ', e a minha idade é: ' + idade + ' anos\n')
console.log(`Meu nome é: ${name}, e a minha idade é: ${idade} anos\n`)

const usuario_short1 = {
    nome: nome,
    idade: idade,
    empresa: "uesb"
}

const usuario_short2 = {
    nome,
    idade,
    empresa: "uesb"
}

console.log(usuario_short1, "\n")
console.log(usuario_short2, "\n")

/**
 * Aula/Atividade do dia 03/09/2021
*/
// // Criando Classes
// // class ToList{
// //     constructor(){
// //         this.data = []
// //     }

// //     addData(name){
// //         this.data.push(name);
// //         console.log(this.data);
// //     }
// // }

// class List{
//     //Construtor
//     constructor(){ 
//         this.data = []
//     }

//     //Método
//     add(name){ 
//         this.data.push(name);
//         console.log(this.data);
//     }
// }

// // Herança
// class ToList extends List{ 
//     constructor(){
//         super();
//         this.user = "Wesley Costa"
//     }

//     getUser(){
//         console.log(this.user)
//     }

//     //Método estático
//     static soma(a,b){ 
//         return a+b;
//         // return this.user;
//     }
// }

// const toList = new ToList()

// //Ação do botão
// document.getElementById("novotodo").onclick = function(){ 
//     // toList.addData('Wesley')
//     toList.add('Wesley')
//     toList.getUser();
// }

// console.log(ToList.soma(1,3))

// // //Tipos de variáveis

// //Var
// var nome1 = "Wesley";
// console.log("Var - Original: " + nome1)

// nome1 = "Wesley S.";
// console.log("Var - Alterado: " + nome1)

// // Forma que possibilita a mudança do valor no const
// const nome = {primeiro_nome: "Wesley"};
// console.log("Const com valor original da pripriedade: " + nome.primeiro_nome)

// nome.primeiro_nome = "João";

// console.log("Mudando o valor da pripriedade no const: " + nome.primeiro_nome)

// //let
// function teste(x){
//     let y = 4;
//     if(x > 2){
//         y = 6;
//         console.log(x,y)
//     }
// }

// console.log(teste(10))
// console.log(y)